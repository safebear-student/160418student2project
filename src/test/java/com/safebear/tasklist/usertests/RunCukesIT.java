package com.safebear.tasklist.usertests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        // 8. Why isn't this running any of my scenarios?
        // because the scenario in feature file is marked as @high-risk, and below, we have tagged not to execute those
        plugin = {"pretty", "html:target/cucumber"},
        tags = "@high-risk",
        glue = "com.safebear.tasklist.usertests",
        features = "classpath:tasklist.features/taskmanagement.feature"
)
public class RunCukesIT {
}

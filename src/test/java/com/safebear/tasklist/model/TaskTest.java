package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

public class TaskTest {

    @Test
    public void creation(){

        LocalDate localDate = LocalDate.now();
        Task task = new Task(1L, "Mop the floors", localDate, false);

        Assertions.assertThat(task.getId()).isEqualTo(1L);

    }

}
